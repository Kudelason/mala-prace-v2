# Getting Started

These instructions will get you a copy of the project up and running
on your local machine for development and testing purposes. Testovani funkcnosti

## Prerequisites

You need Python installed on your machine. You can download Python from [python.org](https://python.org).

## Installing

Follow these steps to get your development environment running:

- Clone the repository:

   ```
   git clone https://gitlab.com/Kudelason/mala-prace.git MyFlaskApp
   ```

- Navigate to the project directory:

   ```
   cd MyFlaskApp
   ```

- Install the required packages:

   ```
   pip install -r requirements.txt
   ```

- Initialize the database:

   ```
   python init_db.py
   ```

- Run the application:

   ```
   flask run
   ```

Open your web browser and visit [http://127.0.0.1:5000](http://127.0.0.1:5000) to
see the application in action.
