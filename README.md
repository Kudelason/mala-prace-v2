# MyFlaskApp

Welcome to MyFlaskApp, a simple web application built with Flask to demonstrate
 basic web development principles using Python.
 This application allows users to register, login, and post short messages.

## Features

- **User Registration:** Sign up to create a new account.
- **User Login:** Log in to access your profile and post messages.
- **Message Posting:** Post and view messages on your timeline.

## Technologies

- Python 3
- Flask
- HTML/CSS
- SQLite for the database

## Getting Started

These instructions will get you a copy of the project up
 and running on your local machine for development and testing purposes.

### Prerequisites

You need Python installed on your machine. You can download Python from [python.org](https://python.org).

### Installing

A step by step series of examples that tell you how to
 get a development environment running.

- Clone the repository:

   ```
   git clone https://gitlab.com/Kudelason/mala-prace-v2.git MyFlaskApp
   ```

- Navigate to the project directory:

   ```
   cd MyFlaskApp
   ```

- Install the required packages:

   ```
   pip install -r requirements.txt
   ```

- Initialize the database:

   ```
   python init_db.py
   ```

- Run the application:

   ```
   flask run
   ```

Open your web browser and visit [http://127.0.0.1:5000](http://127.0.0.1:5000)
 to see the application in action.

### Website

You can also view [website](https://mala-prace-v2-kudelason-eb214907b6ac26c274a507826c5509ba771f392.gitlab.io).

## Contributing

Contributions are what make the open-source community such
 an amazing place to learn, inspire, and create.
  Any contributions you make are **greatly appreciated**.

1. Fork the Project
1. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
1. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
1. Push to the Branch (`git push origin feature/AmazingFeature`)
1. Open a Pull Request

## License

This project is licensed under the MIT License.

## Acknowledgments

- Hat tip to anyone whose code was used
- Inspiration
- etc
