# Welcome to MyFlaskApp

Welcome to MyFlaskApp, a simple web application built with Flask
to demonstrate basic web development principles using Python.
This application allows users to register, login, and post short messages.

## Features

- **User Registration:** Sign up to create a new account.
- **User Login:** Log in to access your profile and post messages.
- **Message Posting:** Post and view messages on your timeline.
